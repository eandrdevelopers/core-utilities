package retry

import "time"

// RetriableFunc is a function capable of being loaded into
// a retry policy
type RetriableFunc func() error

// Policy is a retry policy describing how to
type Policy struct {
	delay                time.Duration
	retries              int
	unrecoverableHandler func(error) bool
}

// WithRetries returns a policy with a retry count.
func (p Policy) WithRetries(count int) Policy {
	p.retries = count
	return p
}

// WithDelay return a policy with a delay between retries.
func (p Policy) WithDelay(delay time.Duration) Policy {
	p.delay = delay
	return p
}

// WithUnrecoverableState returns a policy with a func capable
// of determining an unrecoverable state, and early outing the
// retrial policy.
func (p Policy) WithUnrecoverableState(handler func(error) bool) Policy {
	p.unrecoverableHandler = handler
	return p
}

// Do runs a RetriableFunc with the settings for the policy.
// On failure returns latest error.
func (p *Policy) Do(work RetriableFunc) error {
	err := work()
	// Success means were done here
	if err == nil {
		return nil
	}

	// Check for unrecoverables
	if p.unrecoverableHandler != nil && p.unrecoverableHandler(err) {
		return err
	}

	var currentError error

	for i := p.retries; i > 0; i = i - 1 {
		// Handle Delay
		time.Sleep(p.delay)

		// Retry
		currentError = work()

		// Check for unrecoverables
		if p.unrecoverableHandler != nil && p.unrecoverableHandler(currentError) {
			break
		}
	}

	return currentError
}
