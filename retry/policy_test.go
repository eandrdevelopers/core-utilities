package retry_test

import (
	"errors"
	"strconv"
	"testing"
	"time"

	"bitbucket.org/eandrdevelopers/core-utilities/retry"
	helpers "bitbucket.org/eandrdevelopers/core-utilities/testinghelpers"
)

func TestDo_CalledWithDefaultPolicy_DoesNotRetry(t *testing.T) {
	policy := retry.Policy{}
	calledCount := 0
	expected := 1

	policy.Do(func() error {
		calledCount = calledCount + 1
		return errors.New("test error")
	})

	helpers.Equals(t, expected, calledCount)
}

func TestDo_CalledWithMultipleRetries_RetriesAsExpected(t *testing.T) {
	policy := retry.Policy{}.WithRetries(15)
	calledCount := 0
	expected := 16 // 15 retries plus initial call

	policy.Do(func() error {
		calledCount = calledCount + 1
		return errors.New("test error")
	})

	helpers.Equals(t, expected, calledCount)
}

func TestDo_CalledWithUnrecoverableStateDefined_ExitsAsExpected(t *testing.T) {
	policy := retry.Policy{}.WithRetries(15).WithUnrecoverableState(func(e error) bool {
		return e.Error() == "9"
	})
	calledCount := 0
	expected := 9

	policy.Do(func() error {
		calledCount = calledCount + 1
		return errors.New(strconv.Itoa(calledCount))
	})

	helpers.Equals(t, expected, calledCount)
}

func TestDo_CalledWithUnrecoverableStateDefinedAndInitialRunBeingUnrecoverable_ExitsAsExpected(t *testing.T) {
	policy := retry.Policy{}.WithRetries(15).WithUnrecoverableState(func(e error) bool {
		return e.Error() == "1"
	})
	calledCount := 0
	expected := 1

	policy.Do(func() error {
		calledCount = calledCount + 1
		return errors.New(strconv.Itoa(calledCount))
	})

	helpers.Equals(t, expected, calledCount)
}

func TestDo_CalledWithMultipleRetries_ReturnsLastError(t *testing.T) {
	policy := retry.Policy{}.WithRetries(15)
	calledCount := 0
	expected := "16"

	err := policy.Do(func() error {
		calledCount = calledCount + 1
		return errors.New(strconv.Itoa(calledCount))
	})

	helpers.Equals(t, expected, err.Error())
}

func TestDo_CalledWithRetryOnly_DoesNotDelay(t *testing.T) {
	policy := retry.Policy{}.WithRetries(1)

	now := time.Now()
	policy.Do(func() error {
		return errors.New("test error")
	})
	elapsed := time.Since(now)

	helpers.Assert(t, elapsed > 0 && elapsed < (1*time.Second), "policy took more than a second to run, this should not happen with no delay")
}

func TestDo_CalledWithDelaySpecified_DelaysAsExpected(t *testing.T) {
	policy := retry.Policy{}.WithRetries(2).WithDelay(time.Second * 1)

	now := time.Now()
	policy.Do(func() error {
		return errors.New("test error")
	})
	elapsed := time.Since(now)

	helpers.Assert(t, elapsed > (2*time.Second) && elapsed < (3*time.Second), "with given delay settings, elapsed time fell outside expected delay range.")
}
