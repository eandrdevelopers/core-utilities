package wrappers

import "io"

// Execable is an interface wrapping an object allowing exec calls, such as a database
type Execable interface {
	Exec(query string, args ...interface{}) error
}

// Queryable is an interface wrapping a queryable object, such as a database
type Queryable interface {
	Query(query string, args ...interface{}) (ScannerCloser, error)
}

// ExecableQueryable is a simple combination between Execable and Queryable interfaces
type ExecableQueryable interface {
	Execable
	Queryable
}

// Scanner is an interface wrapping an object which allows scan interation
type Scanner interface {
	Next() bool
	Scan(destination ...interface{}) error
}

// ScannerCloser a simple combination between the Scanner interface and io.Closer
type ScannerCloser interface {
	Scanner
	io.Closer
}

//
type noOpScannerCloser struct {
	Scanner
}

func (noOpScannerCloser) Close() error { return nil }

// NoOpScannerCloser wraps a scanner in a noOp closer.
func NoOpScannerCloser(s Scanner) ScannerCloser {
	return noOpScannerCloser{s}
}
