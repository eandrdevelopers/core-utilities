package models

import (
	"bitbucket.org/eandrdevelopers/core-utilities/wrappers"
	"time"
)

// NetBagAssignment A checking net bag assigned to an invoice.
type NetBagAssignment struct {
	ID         int       `json:"id"`
	Assignment int       `json:"assignment"`
	IsDataSent int       `json:"isDataSent"`
	Prefix     string    `json:"prefix"`
	RouteStop  int       `json:"routeStop"`
	Store      int       `json:"store"`
	Customer   int       `json:"customer"`
	First      string    `json:"first"`
	Last       string    `json:"last"`
	Phone      string    `json:"phone"`
	MarkedIn   time.Time `json:"markedIn"`
}

// ScanInMembers uses a scanner to fill struct members.
func (n *NetBagAssignment) ScanInMembers(scanner wrappers.Scanner) error {
	return scanner.Scan(
		&n.ID,
		&n.Assignment,
		&n.IsDataSent,
		&n.Prefix,
		&n.RouteStop,
		&n.Store,
		&n.Customer,
		&n.First,
		&n.Last,
		&n.Phone,
		&n.MarkedIn,
	)
}
