package logging

import "github.com/sirupsen/logrus"

// Logger represents a logging construct for structure logging.
type Logger interface {
	Log() *logrus.Entry
	Audit() *logrus.Entry
}
