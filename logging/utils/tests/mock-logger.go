package tests

import (
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
)

// MockLogger is a logger which swallows all log request without touching IO calls.
type MockLogger struct {
	drainHandle *logrus.Entry
}

// NewMockLogger Initializes a Mock Logger
func NewMockLogger() *MockLogger {
	logger, _ := test.NewNullLogger()
	return &MockLogger{
		drainHandle: logger.WithField("", ""),
	}
}

// Log returns a logging handle to drain messages
func (logger *MockLogger) Log() *logrus.Entry {
	return logger.drainHandle
}

// Audit returns a logging handle to drain messages
func (logger *MockLogger) Audit() *logrus.Entry {
	return logger.drainHandle
}
