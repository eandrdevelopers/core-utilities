package logging

import (
	"io"
)

// Close allows you to close an io.Closer while still logging
// possible errors
func Close(closer io.Closer, logger Logger) {
	err := closer.Close()
	if err != nil {
		logger.Log().Fatal(err)
	}
}
