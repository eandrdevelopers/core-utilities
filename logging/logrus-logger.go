package logging

import (
	"os"

	"github.com/sirupsen/logrus"
)

func init() {
	logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.SetOutput(os.Stdout)
	logrus.SetLevel(logrus.WarnLevel)
}

// LogrusLogger contains handles to the different types of logging ERSYS supports.
type LogrusLogger struct {
	loggingHandle  *logrus.Entry
	auditingHandle *logrus.Entry
}

// NewLogrusLogger constructs a Logger with all applicable handles.
func NewLogrusLogger(applicationName string) *LogrusLogger {
	base := logrus.WithFields(logrus.Fields{
		"appName": applicationName,
	})
	return &LogrusLogger{
		loggingHandle:  base.WithField("logType", "log"),
		auditingHandle: base.WithField("logType", "audit"),
	}
}

// Log returns the handle for logging to the standard application logs, used
// for standard application level information/errors.
func (logger *LogrusLogger) Log() *logrus.Entry {
	return logger.loggingHandle
}

// Audit returns the handle for logging to the auditing system, used for tracking
// user actions.
func (logger *LogrusLogger) Audit() *logrus.Entry {
	return logger.auditingHandle
}
