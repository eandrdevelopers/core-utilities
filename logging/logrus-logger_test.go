package logging_test

import (
	"testing"

	"bitbucket.org/eandrdevelopers/core-utilities/logging"
	helpers "bitbucket.org/eandrdevelopers/core-utilities/testinghelpers"
)

func TestNewLogrusLogger_Called_SetsUpAuditHandle(t *testing.T) {
	logger := logging.NewLogrusLogger("")

	helpers.Assert(t, logger.Audit() != nil, "Logger audit handle is nil.")
}

func TestNewLogrusLogger_Called_SetsUpLogHandle(t *testing.T) {
	logger := logging.NewLogrusLogger("")

	helpers.Assert(t, logger.Log() != nil, "Logger Logging handle is nil.")
}

func TestNewLogrusLogger_Called_AddsAppnameToLogHandle(t *testing.T) {
	const appName = "Test_App_Name"
	const fieldKey = "appName"
	logger := logging.NewLogrusLogger(appName)

	helpers.Equals(t, appName, logger.Log().Data[fieldKey])
}

func TestNewLogrusLogger_Called_AddsAppnameToAuditHandle(t *testing.T) {
	const appName = "Test_App_Name"
	const fieldKey = "appName"
	logger := logging.NewLogrusLogger(appName)

	helpers.Equals(t, appName, logger.Audit().Data[fieldKey])
}

func TestNewLogrusLogger_Called_AddsLogTypeToAuditHandle(t *testing.T) {
	const expected = "audit"
	const fieldKey = "logType"
	logger := logging.NewLogrusLogger("")

	helpers.Equals(t, expected, logger.Audit().Data[fieldKey])
}

func TestNewLogrusLogger_Called_AddsLogTypeToLogHandle(t *testing.T) {
	const expected = "log"
	const fieldKey = "logType"
	logger := logging.NewLogrusLogger("")

	helpers.Equals(t, expected, logger.Log().Data[fieldKey])
}
